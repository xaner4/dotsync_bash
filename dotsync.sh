#!/usr/bin/env bash
set -e


dotfile_path=$HOME/.dotfiles


function main () {
    # Gets Arguments and check them with case 
    case $1 in
        init)
            git_init;;
        new)
            shift
            new_env $*;;
        add)
            shift
            add_file $*;;
        rm)
            shift
            remove_file $*;;
        diff)
            diff_files;;
        sync)
            sync_local_git;;
        change)
            shift
            change_env $*;;
        list)
            list_env;;
        *|help)
            usage;;
    esac
}

function usage() {
    # Print help page
    cat << EOF
        HELP
EOF
}

function git_init() {
    if [ ! -d $dotfile_path/.git ]; then
        git init
        echo -e "Git remote for syncing to: "
        read git_remote
        if [ -z $git_remote ]; then
            echo "No git remote spesified"
            git_init
        fi

        git remote add origin $git_remote 2> /dev/null
        valid_repo=$(git fetch 2> /dev/null; echo $?)
        if [ ! $valid_repo == 0 ]; then
            echo -e "${yellow}$git_remote ${white}does not appear to be a git repository"
            git remote remove origin
            git_init
        fi

        # 
        origin=$(git remote show)
        remote=$(git remote get-url $origin)

        if [ $? -eq 0 ]; then
            echo -e "Remote added${green} $origin =${yellow} $remote"
        else
            echo -e "Something went wrong"
        fi

    else
        origin=$(git remote show)
        if [ -z $origin ]; then
            rm -r $dotfile_path/.git
            git_init
        fi
        remote=$(git remote get-url $origin)
        echo -e "A remote already exsists:${green} $origin =${yellow} $remote"
    fi
}

function new_env() {
    # If $1 is not a empty string
    if [ ! -z $1 ]; then
        git checkout -b $1
    else
        echo -e "No name provided ${yellow}Add name"
    fi
}

function list_env() {
    result=$(git branch --list)
    echo $result
}

function add_file() {
    # If $1 is not a empty string
    if [ ! -z $1 ]; then
        # If $1 is a valid file
        if [ -f $1 ]; then
            cp -av $1 $dotfile_path
        else
            echo -e "Not a valid file path"
        fi
    else
        echo -e "No file path provided"
    fi
}

function remove_file() {
    # If $1 is not a empty string
    if [ ! -z $1 ]; then
        # If $1 is a valid file path in the $dotfile_path
        if [ -f $dotfile_path/$1 ]; then
            echo -e ""
            rm -rv $dotfile_path/$1
        fi
    else
        echo "No file name proided"
    fi
}



# Finds the folder where the script is stored
_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
if [ $_path != "" ]; then
    # changes directory to the scirpt path and sources colors script
    cd $_path
    source ./colors.sh

    # Checks if $dotfile_path, if not make it
    if [ ! -d $dotfile_path ]; then
        mkdir -pv $dotfile_path
    fi
    cd $dotfile_path
    main $*
fi