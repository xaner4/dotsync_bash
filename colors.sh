#!/usr/bin/env bash
set -e

# Regular
Black='\e[0;30m'     # Black
red='\e[0;31m'       # Red
green='\e[0;32m'     # Green
yellow='\e[0;33m'    # Yellow
blue='\e[0;34m'      # Blue
purple='\e[0;35m'    # Purple
cyan='\e[0;36m'      # Cyan
white='\e[0;37m'     # White

# Bold
b_Black='\e[1;30m'   # Black
b_red='\e[1;31m'     # Red
b_green='\e[1;32m'   # Green
b_yellow='\e[1;33m'  # Yellow
b_blue='\e[1;34m'    # Blue
b_purple='\e[1;35m'  # Purple
b_cyan='\e[1;36m'    # Cyan
b_white='\e[1;37m'   # White

# Underline
u_Black='\e[4;30m'   # Black
u_red='\e[4;31m'     # Red
u_green='\e[4;32m'   # Green
u_yellow='\e[4;33m'  # Yellow
u_blue='\e[4;34m'    # Blue
u_purple='\e[4;35m'  # Purple
u_cyan='\e[4;36m'    # Cyan
u_white='\e[4;37m'   # White

# Background
bck_Black='\e[40m'   # Black
bck_red='\e[41m'     # Red
bck_green='\e[42m'   # Green
bck_yellow='\e[43m'  # Yellow
bck_blue='\e[44m'    # Blue
bck_purple='\e[45m'  # Purple
bck_cyan='\e[46m'    # Cyan
bck_white='\e[47m'   # White
bck_reset='\e[0m'    # Text Reset
